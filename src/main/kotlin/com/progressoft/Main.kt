package com.progressoft

import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material.*
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color.Companion.Black
import androidx.compose.ui.graphics.Color.Companion.Blue
import androidx.compose.ui.graphics.Color.Companion.Red
import androidx.compose.ui.window.Window
import androidx.compose.ui.window.application
import java.io.BufferedReader
import java.io.InputStreamReader
import kotlin.concurrent.thread


var isLoading = mutableStateOf(true);
var pods = mutableStateListOf<String>()
var portForwardedPods = mutableStateListOf<String>()
var loadedPods = false;

@Composable
fun App() {
    var selectedPod by remember { mutableStateOf("") }
    Scaffold(
        floatingActionButton = {
            if (selectedPod.isNotEmpty()) {
                val isPortForwardPort = portForwardedPods.contains(selectedPod)
                FloatingActionButton(
                    onClick = {
                        selectedPod = if (isPortForwardPort) {
                            killThread(selectedPod.split("$")[1]);
                            portForwardedPods.remove(selectedPod)
                            pods.add(selectedPod.split("$")[0])
                            "";
                        } else {
                            val portForward = portForward(selectedPod)
                            portForwardedPods.add("$selectedPod$$portForward")
                            pods.remove(selectedPod)
                            "";
                        }
                    }, backgroundColor = if (isPortForwardPort) Red else Blue
                ) {}
            }
        },
    ) {
        Column {
            LazyColumn(
                Modifier.weight(0.3F).fillMaxSize()
            ) {
                items(portForwardedPods) {
                    Row(verticalAlignment = Alignment.CenterVertically) {
                        RadioButton(
                            selected = selectedPod == it,
                            onClick = { selectedPod = it },
                        )
                        Text(it)
                    }
                }

            }
            Divider(color = Black)
            LazyColumn(
                Modifier.weight(0.7f).fillMaxSize()
            ) {

                items(pods) {
                    Row(verticalAlignment = Alignment.CenterVertically) {
                        RadioButton(
                            selected = selectedPod == it,
                            onClick = { selectedPod = it },
                        )
                        Text(it)
                    }

                }

            }

        }
    }
}

fun loadPods() {
    thread {
        val command = "kubectl get pods -n infra-services"
        val processBuilder = ProcessBuilder(command.split("\\s".toRegex()))
        processBuilder.redirectErrorStream(true)
        val process = processBuilder.start()
        val reader = BufferedReader(InputStreamReader(process.inputStream))
        reader.readLines().forEach {
            if (it.startsWith("central-oracle")) {
                pods.add(it.split("\\s".toRegex())[0])
            }
        }
        isLoading.value = false
        loadedPods = true;
    }

}

fun main() = application {
    Window(onCloseRequest = {
        portForwardedPods.forEach {
            val port = it.split("$")[1]
            killThread(port)
        }
        exitApplication()
    }) {
        if (!loadedPods) {
            loadPods()
        }
        if (isLoading.value) {
            IndeterminateCircularIndicator()
        } else {
            App()
        }
    }
}

@Composable
fun IndeterminateCircularIndicator() {
    Column(
        modifier = Modifier.fillMaxSize(),
        verticalArrangement = Arrangement.Center,
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        CircularProgressIndicator()
    }
}


fun portForward(pod: String): Int {
    val port = checkAvailablePort(1522)
    thread(name = pod) {
        val command = "kubectl port-forward $pod $port:1521 -n infra-services"
        val processBuilder = ProcessBuilder(command.split("\\s".toRegex()))
        processBuilder.redirectErrorStream(true)
        val process = processBuilder.start()
    }
    return port;
}

fun checkAvailablePort(port: Int): Int {
    val command = "lsof -t -i:$port"
    val processBuilder = ProcessBuilder(command.split("\\s".toRegex()))
    processBuilder.redirectErrorStream(true)
    val process = processBuilder.start()
    val reader = BufferedReader(InputStreamReader(process.inputStream))
    reader.readLines().ifEmpty {
        return port;
    }
    return checkAvailablePort(port + 1)
}

fun killThread(port: String) {
    val threadId = getThreadId(port)
    val command = "kill -9 $threadId"
    val processBuilder = ProcessBuilder(command.split("\\s".toRegex()))
    processBuilder.redirectErrorStream(true)
    val process = processBuilder.start()
    val reader = BufferedReader(InputStreamReader(process.inputStream))
    reader.readLines().forEach {
        println(it)
    }
}

fun getThreadId(port: String): String {
    val command = "lsof -t -i:$port"
    val processBuilder = ProcessBuilder(command.split("\\s".toRegex()))
    processBuilder.redirectErrorStream(true)
    val process = processBuilder.start()
    val reader = BufferedReader(InputStreamReader(process.inputStream))
    return reader.readLine() ?: ""
}
